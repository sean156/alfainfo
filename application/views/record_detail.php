<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>維修紀錄資訊 | <?=SITE_NAME?></title>
<!-- BOOTSTRAP STYLES-->
<link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME ICONS STYLES-->
<link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
<!--CUSTOM STYLES-->
<link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" />
<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<?php include_once("block_nav.php"); ?>
	<div id="page-wrapper" class="page-wrapper-cls">
		<div id="page-inner">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="page-head-line">維修紀錄資訊</h1>
				</div>
			</div>
			<div class="well">
				<h3>Jumbotron heading</h3>
				<p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
				<p><a class="btn btn-lg btn-success" href="<?=base_url("record")?>"
					role="button">Back</a></p>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading text-center">零件</div>
						<table class="table table-striped">
							<thead>
							<tr class="success">
								<th>零件代號</th>
								<th>零件名稱</th>
								<th>數量</th>
								<th>單價</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($results as $key => $value): ?>
								<?php if ($value['spare_part_id'] > 0): ?>
							<tr>
								<td><?=$value['spare_code']?></td>
								<td><?=$value['spare_name']?></td>
								<td><?=$value['spare_part_quantity']?></td>
								<td><?=$value['spare_price']?></td>
							</tr>
								<?php endif; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading text-center">工資</div>
						<table class="table table-striped">
							<thead>
							<tr class="info">
								<th>修護內容</th>
								<th>價格</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($results as $key2 => $value2): ?>
								<?php if (!empty($value2['working_item'])): ?>
							<tr>
								<td><?=$value2['working_item']?></td>
								<td><?=$value2['working_price']?></td>
							</tr>
								<?php endif; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	    </div>
	</div>
</div>

<?php include_once("block_footer.php"); ?>

<script src="<?=base_url();?>assets/js/jquery-1.11.1.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.js"></script>
<script src="<?=base_url();?>assets/js/jquery.metisMenu.js"></script>
<script src="<?=base_url();?>assets/js/custom.js"></script>
</body>
</html>