<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("Users.php");

class Record extends Users {

	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
		$this->load->model('record_model');
		$this->load->model('spare_model');
		//$this->load->library('zebra_pagination');
	}

	public function index($keyword = "")
	{
		$this->load->library('zebra_pagination');
		//$this->form_validation->set_rules('code', '代號', 'required');

		$where = "";
		$bind = array();
		$orderby = "id DESC";

		//search
		if (!empty($keyword)) {
			$where2 = "";
			//spare
			$where3 = "title LIKE ? OR code LIKE ?";
			$bind3[] = "%".$keyword."%";
			$bind3[] = "%".$keyword."%";
			$result_row3 = $this->spare_model->getSpare($where3, $bind3);
			if (!empty($result_row3)) {
				for ($i = 0; $i < count($result_row3); $i++) {
					$where2 .= "spare_part_id = ? AND ";
					$bind2[] = $result_row3[$i]['id'];
				}
			}
			if (!empty($where2)) {
				$where2 = substr($where2, 0, strlen($where2) - 4);
				$where2 = "(".$where2.") OR ";
			}

			//detail
			$where2 .= "working_item LIKE ? ";
			$bind2[] = "%".$keyword."%";
			$result_row2 = $this->record_model->getRecordDetail($where2, $bind2);

			if (!empty($result_row2)) {
				for ($i = 0; $i < count($result_row2); $i++) {
					$where .= "working_no = ? AND ";
					$bind[] = $result_row2[$i]['working_no'];
				}
			}

			if (!empty($where)) {
				$where = substr($where, 0, strlen($where) - 4);
				$where = "(".$where.") OR ";
			}

			$where .= "(working_no LIKE ? OR mileage LIKE ?) ";
			$bind[] = "%".$keyword."%";
			$bind[] = "%".$keyword."%";
		}

		//how many records should be displayed on a page?
		$records_per_page = 5;

		$this->zebra_pagination->padding(false);
		$this->zebra_pagination->method('url');
		$limit = (($this->zebra_pagination->get_page() - 1) * $records_per_page).", ".$records_per_page;

		$data['records'] = $this->record_model->getRecord($where, $bind, $orderby, $limit);
		$total = $this->record_model->countRecord($where, $bind);

		if ($total > 0) {
		    //pass the total number of records to the pagination class
		    $this->zebra_pagination->records($total);

		    //records per page
		    $this->zebra_pagination->records_per_page($records_per_page);

		    //render the pagination links
		    $page = $this->zebra_pagination->render(true);

		    $data['page'] = $page;
		}

		$this->load->view('record_list', $data);
	}

	public function add()
	{
		//零件下拉選單
		$spares = $this->spare_model->getSpare("", array(), "title ASC");
		$data['spares'] = $spares;

		$this->load->view("record_add", $data);
	}

	public function create() {
		$this->form_validation->set_rules('working_no', '工作單號', 'required');
		$this->form_validation->set_rules('mileage', '哩程', 'required');
		
		if ($this->form_validation->run() === FALSE) {
			//零件下拉選單
			$spares = $this->spare_model->getSpare("", array(), "title ASC");
			$data['spares'] = $spares;

			$this->load->view("record_add", $data);
		} else {
			$this->escapePost();

			$data['working_no'] = $_POST['working_no'];
			$data['mileage'] = $_POST['mileage'];
			$data['ctime'] = date("Y-m-d H:i:s");
			$data['arrive_time'] = $_POST['arrive_time'];
	        $data['depart_time'] = $_POST['depart_time'];
    	    $data['note'] = $_POST['note'];

	        $insert_id = $this->record_model->addRecord($data); //新增主檔

	        $flag = false;

    	    if ($insert_id > 0) {
        	//新增成功

				if (!empty($_POST['spare_id_add'])) {
					//維修項目新增
					$bind_section = array();
					for ($i = 0; $i < count($_POST['spare_id_add']); $i++) {
						$bind_section[$i]['spare_part_id'] = $_POST['spare_id_add'][$i];
						$bind_section[$i]['spare_part_quantity'] = $_POST['spare_part_quantity_add'][$i];
						$bind_section[$i]['spare_price'] = $_POST['spare_price_add'][$i];
						$bind_section[$i]['working_item'] = $_POST['working_item_add'][$i];
						$bind_section[$i]['working_price'] = $_POST['working_price_add'][$i];
						$bind_section[$i]['ctime'] = date("Y-m-d H:i:s");
						$bind_section[$i]['working_no'] = $_POST['working_no'];
					}

					$res = $this->record_model->addRecordDetail($bind_section);

					if ($res) {
						$flag = true;
					} else {
						$error_txt = "段落新增失敗！";
					}
				}
			} else {
				$error_txt = "文章新增失敗！";
			}

			if ($flag) {
				redirect_header(base_url("record"), "新增完成！", "3");
			} else {
				redirect_header(base_url("record/add"), $error_txt, "3");
			}
		}
	}

	public function edit($id)
	{
		//零件下拉選單
		$spares = $this->spare_model->getSpare("", array(), "title ASC");
		$data['spares'] = $spares;

		//主檔
		$where = "id = ?";
		$bind = array(0 => $id);
		$row = array();
		$result_array = $this->record_model->getRecord($where, $id);
		if (is_array($result_array)) {
			$row = $result_array[0];
		}
		$data['row'] = $row;

		//detail
		$detail = array();
		if (!empty($row)) {
			$where = "working_no = ?";
			$bind = array(0 => $row['working_no']);
			$orderby = "id DESC";
			$detail = $this->record_model->getRecordDetail($where, $bind, $orderby);
		}
		$data['detail'] = $detail;
			
		$this->load->view("record_edit", $data);
	}

	public function modify()
    {
        $this->escapePost(); //過濾 post

        //圖文更新
        for ($i = 0; $i < count($_POST['update_value']); $i++) {
            //修改時不需要判斷是不是空值, 因為空值代表刪掉
            $data['spare_part_id'] = $_POST['update_value'][$i]['spare_id'];
            $data['spare_part_quantity'] = $_POST['update_value'][$i]['spare_part_quantity'];
            $data['spare_price'] = $_POST['update_value'][$i]['spare_price'];
            $data['working_item'] = $_POST['update_value'][$i]['working_item'];
            $data['working_price'] = $_POST['update_value'][$i]['working_price'];
            $data['mtime'] = date("Y-m-d H:i:s");

            $where = "id = ?";
            $bind_where = array(0 => $_POST['update_value'][$i]['detail_id']);

            if ($this->record_model->modifyRecordDetail($where, $bind_where, $data)) {
                $flag_update = true;
            } else {
                $flag_update = false;
            }
        }

        if (!empty($_POST['spare_id_add'])) {
			//維修項目新增
            $p = count($_POST['update_value']); //圖文新增的檔案是從更新陣列長度的數量開始
			for ($i = 0; $i < count($_POST['spare_id_add']); $i++) {
				$bind_section[$i]['spare_part_id'] = $_POST['spare_id_add'][$i];
				$bind_section[$i]['spare_part_quantity'] = $_POST['spare_part_quantity_add'][$i];
				$bind_section[$i]['spare_price'] = $_POST['spare_price_add'][$i];
				$bind_section[$i]['working_item'] = $_POST['working_item_add'][$i];
				$bind_section[$i]['working_price'] = $_POST['working_price_add'][$i];
				$bind_section[$i]['ctime'] = date("Y-m-d H:i:s");
				$bind_section[$i]['working_no'] = $_POST['working_no'];
                $p++;

			}

			$res = $this->record_model->addRecordDetail($bind_section);

            if ($res > 0) {
                $flag_add = true;
            } else {
                $flag_add = false;
            }
        }

        //更新主檔
        $data2['mileage'] = $_POST['mileage'];
        $data2['arrive_time'] = $_POST['arrive_time'];
        $data2['depart_time'] = $_POST['depart_time'];
        $data2['note'] = $_POST['note'];
        $data2['mtime'] = date("Y-m-d H:i:s");

        $where = "id = ?";
        $bind_where = array(0 => $_POST['record_id']);

        $this->record_model->modifyRecord($where, $bind_where, $data2);

        $this->redirectHeader(base_url("record"), "修改完成！", "3");
    }

	public function delete($working_no)
	{
		$where = "working_no = ?";
		$bind = array(0 => $working_no);

		$affected_rows = $this->record_model->deleteRecord($where, $bind);

		if ($affected_rows > 0) {
			$txt = "刪除完成！";
		} else {
			$txt = "刪除失敗！";
		}

		$this->redirectHeader(base_url("record"), $txt, "3");
	}

    public function keyword($keyword)
    {
		$keyword = urldecode($keyword);
    	$this->index($keyword);
    }

	public function detail($working_no)
	{
		$where = "working_no = ?";
		$bind = array(0 => $working_no);
		$orderby = "id ASC";

		$results = $this->record_model->getRecordDetail($where, $bind, $orderby);

		//轉換零件資料
		for ($i = 0; $i < count($results); $i++) {
			$where2 = "id = ?";
			$bind2 = array(0 => $results[$i]['spare_part_id']);
			$spare_array = $this->spare_model->getSpare($where2, $bind2);
			if (!empty($spare_array)) {
				$results[$i]['spare_code'] = $spare_array[0]['code'];
				$results[$i]['spare_name'] = $spare_array[0]['title'];
			} else {
				$results[$i]['spare_code'] = "";
				$results[$i]['spare_name'] = "";
			}
		}
		
		$data['results'] = $results;

		$this->load->view("record_detail", $data);
	}
}