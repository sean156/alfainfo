<?php
////////////////////////////////////////////////////
// Watch project - backend
//
// article module for backend
//
// Copyright (C) 2016  Sean Bao
////////////////////////////////////////////////////

define("IN_WBS", true);
include_once("rwd_common.php");

header("Content-Type:text/html; charset=utf-8");

$ano = $_POST['ano'];
$where = "SEQNO = ?";
$stmt_value = array(0 => $ano);
$result = $db->read("ARTICLE", $where, $stmt_value);
$row = $result[0];

if (!empty($row)) {
    $folder_name = get_image_foldername($row['KIND']); //取得文章分類對應的圖檔資料夾名稱

    $image_big_upload_path = $site['article_source_image_dir'].$folder_name."/";
    $image_upload_path = $site['article_image_dir'].$folder_name."/";

    //建資料夾
    if (!file_exists($site['article_image_dir'])) { //縮圖資料夾
        mkdir($site['article_image_dir'], 0775);
    }

    if (!file_exists($image_upload_path)) { //個別分類縮圖資料夾
        mkdir($image_upload_path, 0775);
    }

    if (!file_exists($site['article_source_image_dir'])) { //原圖資料夾
        mkdir($site['article_source_image_dir'], 0775);
    }

    if (!file_exists($image_big_upload_path)) { //個別分類原圖資料夾
        mkdir($image_big_upload_path, 0775);
    }

    $img_flag1 = false;
    if(!empty($_FILES['filename']['name'])) {
        //rename image
        $r = date("YmdHis");
        $pic1_new_name = "hni_".$r."_".substr(rand(), 0, 2);
        $ext = pathinfo($_FILES['filename']['name'], PATHINFO_EXTENSION); //副檔名
        $pic1_big_imagename = $pic1_new_name.".".$ext;

        $img_flag1 = move_uploaded_file($_FILES['filename']['tmp_name'], $image_big_upload_path.$pic1_big_imagename);

        if ($img_flag1) {
            $pic1_src = $site['article_source_image_src'].$folder_name."/".$pic1_big_imagename;

            if (!empty($_POST['old_image'])) {
                @unlink($_SERVER['DOCUMENT_ROOT']."/".$_POST['old_image']);
            }

            $sql = "UPDATE ARTICLE SET home_news_image = ? WHERE SEQNO = ?";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(1, $pic1_src, PDO::PARAM_STR, 100);
            $stmt->bindParam(2, $ano, PDO::PARAM_INT, 11);
            $stmt->execute();

            if($stmt->rowCount() > 0) {
                $err_str = "";
            } else {
                $err_str = "系統忙碌中!";
            }
        } else {
            $err_str = "上傳失敗!";
        }
    } else {
        $err_str = "請選擇圖檔!";
    }
} else {
	$err_str = "無文章資料!";
}

if(empty($err_str)) {
    echo $ano;
} else {
	echo "-1";
}

$db = null;
?>
