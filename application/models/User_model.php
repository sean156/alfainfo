<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

	public function getUserByLogin($email, $password)
	{
		$sql = "SELECT id, email, password, is_enable FROM users WHERE email = ? AND password = ?";
        $query = $this->db->query($sql, array($email, md5($password)));
        $row = $query->row_array();
        if (empty($row)) {
            return 0;
        } else {
            return $row;
        }
	}
}