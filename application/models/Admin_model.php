<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_admin_by_login($account, $password)
    {
        $sql = "SELECT no, account, password FROM tluxy_admin WHERE account = ? AND password = ?";
        $query = $this->db->query($sql, array($account, md5($password)));
        $row = $query->row_array();
        if (empty($row)) {
            return 0;
        } else {
            return $row;
        }
    }
}