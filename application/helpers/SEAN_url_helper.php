<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('redirect_header'))
{
	function redirect_header($url, $message = "", $time = "0")
    {
        $url = preg_replace("/&amp;/i", '&', htmlspecialchars($url, ENT_QUOTES));
        echo '
        <!DOCTYPE html>
        <html lang="zh-Hant">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="refresh" content="'.$time.';url='.$url.'">
        <title>redirect page</title>
        <style type="text/css">
            body {background-color : #fcfcfc; font-size: 12px; font-family: Trebuchet MS,Verdana, Arial, Helvetica, sans-serif; margin: 0px;}
            .redirect {width: 70%; margin: 0 auto; text-align: center; padding: 15px; border: #e0e0e0 1px solid; color: #666666; background-color: #f6f6f6;}
            .redirect a:link {color: #666666; text-decoration: none; font-weight: bold;}
            .redirect a:visited {color: #666666; text-decoration: none; font-weight: bold;}
            .redirect a:hover {color: #999999; text-decoration: underline; font-weight: bold;}
        </style>
        </head>
        <body>';

        if ($message != "") {
            echo '
            <div class="redirect">
              <span style="font-size: 16px; font-weight: bold;">'.$message.'</span>
              <hr style="height: 3px; border: 3px #FF9900 solid; width: 95%;">
              <p>If your browser does not support meta redirection, please click <a href="'.$url.'">HERE</a>
                to be redirected</p>
            </div>';
        }

        echo '
        </body>
        </html>';
        die();
    }
}