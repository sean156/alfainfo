<?php
////////////////////////////////////////////////////
// Watch project - backend
//
// main php for backend
//
// Copyright (C) 2016  Sean Bao
////////////////////////////////////////////////////
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

define("IN_WBS", true);
include_once("rwd_common.php");

//include the pagination class
include_once("rwd/includes/Zebra_Pagination/Zebra_Pagination.php");

//instantiate the pagination object
$pagination = new Zebra_Pagination();
$pagination->padding(false);

//assign 後台共用的參數
$home_block_name = array(
    array('id' => "latestnews1", 'name' => "最新消息 4-1"),
    array('id' => "latestnews2", 'name' => "最新消息 4-2"),
    array('id' => "latestnews3", 'name' => "最新消息 4-3"),
    array('id' => "latestnews4", 'name' => "最新消息 4-4"),
    array('id' => "editor", 'name' => "編輯精選"),
    array('id' => "news", 'name' => "新聞活動"),
    array('id' => "review", 'name' => "鐘錶評測"),
    array('id' => "guide", 'name' => "賞表指南"),
    array('id' => "report", 'name' => "站長報告"),
    array('id' => "blog", 'name' => "鐘錶部落"),
    array('id' => "style", 'name' => "時尚風格"),
    array('id' => "daily", 'name' => "每日精選"));

$tpl->assign("home_block_name", $home_block_name); //首頁區塊名稱

//接值
$aid = $_POST['aid'];
$pwd = $_POST['pwd'];
$op = (!empty($_POST['op']) ? $_POST['op'] : $_GET['op']);
$dop = (!empty($_POST['dop']) ? $_POST['dop'] : $_GET['dop']);
$module = (!empty($_POST['module']) ? $_POST['module'] : $_GET['module']);

//login
if (!empty($aid) && !empty($pwd) && ($op == "login")) {
    if ($aid != "" && $pwd != "") {
        $stmt_value = array(0 => $aid, 1 => MD5($pwd));
        $db->PrepareSelect($site['prefix']."_authors", "no, password, enable, account", "account=? AND password=?", $stmt_value);
        $row0 = $db->FetchAssoc();

        if ($row0['enable'] > 0) {
            //write to session
            $_SESSION['admin'] = base64_encode($row0['account'].":".$pwd);
            //header("Location: ".$_SERVER['PHP_SELF']);
            redirect_header($_SERVER['PHP_SELF']);
        } else {
            //header("Location: ".$_SERVER['PHP_SELF']."?err=loginfail");
            redirect_header($_SERVER['PHP_SELF']."?err=loginfail");
        }
        unset($op);
    }
}

if (!isset($dop)) $dop = "adminMain";

//check login
$account_id = is_admin();
//echo "dop=$dop";
if ($account_id > 0) {

    //舊後台登入機制
    $admin = unserialize($_SESSION['ss_account']);
    $admin_id = $admin->account_id;

    switch ($dop) {
        case "logout":
            unset($_SESSION['admin']);
            session_destroy();
            //header("Location: ".$_SERVER['PHP_SELF']);
            redirect_header($_SERVER['PHP_SELF']);
            break;

        //default page
        case "adminMain":
            adminMain();
            break;

        //include modules
        default:
            $casedir = dir("rwd/admin");
            while($func = $casedir->read()) {
                $f_array = explode("_", $func);
                $module_name = str_replace(".php", "", $f_array[1]);
                if(substr($func, 0, 6) == "admin_" ) {
                    //只載入 $dop 的模組
                    if($module == $module_name) {
                        include($casedir->path."/$func");
                    }
                }
            }
            closedir($casedir->handle);
            break;
    }

    $content = $tpl->fetch("admin_page_index.html");
} else {
    //$content = $tpl->fetch("admin_page_login.html");
    header("Location: backend/login.php");
}

//output
ob_start();
//print_r($_SESSION);
echo $content;

ob_end_flush();

$db = null;

//===============
// main function
//===============

/**
 * default page
 *
 */
function adminMain()
{
    $GLOBALS['tpl']->assign("admin_body", $GLOBALS['tpl']->fetch("admin_block_home.html"));
}

//=================
// common function
//=================

function is_admin()
{
    if (!empty($_SESSION['ss_account'])) {
        return 1;
    }
    return 0;
}

/**
 * 品牌
 */
function get_brands()
{
    $results = $GLOBALS['db']->read("BRAND", $where, $stmt_value, $orderby, $limit);
}

//舊後台登入機制修正
class Account
{
    var $account_id;
}
