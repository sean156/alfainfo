<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>修改零件 | <?=SITE_NAME?></title>
<!-- BOOTSTRAP STYLES-->
<link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME ICONS STYLES-->
<link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
<!--CUSTOM STYLES-->
<link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" />
<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<?php include_once("block_nav.php"); ?>
	<div id="page-wrapper" class="page-wrapper-cls">
		<div id="page-inner">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="page-head-line">編輯零件</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<form role="form" class="form-horizontal" action="<?=base_url()?>spare/modify"
							method="post">
							<input type="hidden" name="id" value="<?=$row['id']?>">
								<?=validation_errors()?>
								<div class="form-group">
									<label class="col-xs-2 control-label">零件名稱</label>
									<div class="col-xs-10">
										<input type="text" name="title" class="form-control"
										placeholder="零件名稱" value="<?=$row['title']?>">
										<!--p class="help-block">Example block-level help text here.</p-->
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">零件代號</label>
									<div class="col-xs-10">
										<input type="text" name="code" class="form-control"
										placeholder="零件代號" value="<?=$row['code']?>">
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-offset-2 col-xs-10">
										<button type="submit" class="btn btn-default">送出</button>
									<!--/div>
									<div class="col-xs-8"-->
										<button type="button" class="btn btn-default"
										onclick="location.href='<?=base_url()?>spare';">返回</button>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
	    </div>
	</div>
</div>

<?php include_once("block_footer.php"); ?>

<script src="<?=base_url();?>assets/js/jquery-1.11.1.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.js"></script>
<script src="<?=base_url();?>assets/js/jquery.metisMenu.js"></script>
<script src="<?=base_url();?>assets/js/custom.js"></script>
</body>
</html>