<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spare_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function getSpare($where = "", $bind = array(), $orderby = "id DESC", $limit = "")
    {
        $column_name = $this->getColumnName("spare_part");

        $sql = "SELECT ".$column_name." FROM spare_part ";
        if (!empty($where)) {
        	$sql .= " WHERE ".$where;
        }
        if (!empty($orderby)) {
        	$sql .= " ORDER BY ".$orderby;
        }
        if (!empty($limit)) {
        	$sql .= " LIMIT ".$limit;
        }

		if (empty($bind)) {
	        $query = $this->db->query($sql);
	    } else {
	    	$query = $this->db->query($sql, $bind);
	    }

        $result = $query->result_array();

        if (empty($result)) {
            return 0;
        } else {
            return $result;
        }
    }

    public function countSpare($where = "", $bind = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM spare_part ";
        if (!empty($where)) {
        	$sql .= "WHERE ".$where;
        }

        if (empty($bind)) {
	        $query = $this->db->query($sql);
	    } else {
	    	$query = $this->db->query($sql, $bind);
	    }

        $result = $query->result_array();

        if (empty($result)) {
            return 0;
        } else {
            return $result[0]['total'];
        }
    }

	public function addSpare($data) {
		$sql = "INSERT INTO spare_part SET ";

		if (empty($data)) {
			return 0;
		}

		foreach ($data AS $key => $value) {
			$sql .= $key."= ?,";
			$bind[] = $value;
		}
		$sql = substr($sql, 0, strlen($sql) - 1);

		$this->db->query($sql, $bind);

		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		} else {
			return 0;
		}
	}

	public function modifySpare($where, $bind_where, $data) {
		$sql = "UPDATE spare_part SET ";

		if (empty($where)) {
			return 0;
		}

		foreach ($data AS $key => $value) {
			$sql .= $key."= ?,";
			$bind[] = $value;
		}

		if (!empty($bind_where)) {
			array_push($bind, $bind_where);
		}

		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= " WHERE ".$where;

		$this->db->query($sql, $bind);

		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return 0;
		}
	}

    public function getColumnName($table_name)
    {
    	$column_name = "";
    	$sql = "SHOW COLUMNS FROM ".$table_name;
		$query = $this->db->query($sql);
		$result = $query->result_array();

		if (!empty($result)) {
			foreach($result as $key => $value) {
				$column_name .= $value['Field'].',';
			}
			$column_name = substr($column_name, 0, strlen($column_name) - 1);
		}

		return $column_name;
	}
}