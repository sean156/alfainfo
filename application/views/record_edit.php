<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>修改維修紀錄 | <?=SITE_NAME?></title>
<!-- BOOTSTRAP STYLES-->
<link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME ICONS STYLES-->
<link href="<?=base_url()?>assets/css/font-awesome.css" rel="stylesheet" />
<!--CUSTOM STYLES-->
<link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" />
<link rel="stylesheet" href="<?=base_url()?>assets/js/datetimepicker/jquery.datetimepicker.css">
<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<?php include_once("block_nav.php"); ?>
	<div id="page-wrapper" class="page-wrapper-cls">
		<div id="page-inner">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="page-head-line">修改維修紀錄</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<form role="form" class="form-horizontal" method="post"
							action="<?=base_url('record/modify')?>">
							<input type="hidden" name="record_id" value="<?=$row['id']?>">
							<input type="hidden" name="working_no" value="<?=$row['working_no']?>">
								<?=validation_errors()?>
								<div class="form-group">
									<label class="col-xs-2 control-label">工作單號</label>
									<div class="col-xs-10">
										<p><strong><?=$row['working_no']?></strog></p>
										<!--p class="help-block">Example block-level help text
										here.</p-->
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">哩程</label>
									<div class="col-xs-10">
										<input type="text" name="mileage" class="form-control"
										value="<?=$row['mileage']?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">進場時間</label>
									<div class="col-xs-10">
										<input type="text" name="arrive_time" class="form-control"
										id="datetimepicker1" value="<?=$row['arrive_time']?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">出場時間</label>
									<div class="col-xs-10">
										<input type="text" name="depart_time" class="form-control"
										id="datetimepicker2" value="<?=$row['depart_time']?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">備註</label>
									<div class="col-xs-10">
										<textarea name="note" class="form-control"
										><?=$row['note']?></textarea>
									</div>
								</div>
								<?php for ($i = 0; $i < count($detail); $i++): ?>
								<hr class="hr-primary">
								<input type="hidden" name="update_value[<?=$i?>][detail_id]"
									value="<?=$detail[$i]['id']?>">
								<div class="form-group">
									<label class="col-xs-2 control-label">零件名稱</label>
									<div class="col-xs-6">
										<select name="update_value[<?=$i?>][spare_id]"
										class="form-control">
											<option value="">== 請選擇 ==</option>
											<?php foreach ($spares AS $key => $value) : ?>
											<option value="<?=$value['id']?>"
						<?php if ($detail[$i]['spare_part_id'] == $value['id']) echo "selected"; ?>
											><?=$value['title']?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">數量</label>
									<div class="col-xs-6">
										<input type="text" class="form-control"
										name="update_value[<?=$i?>][spare_part_quantity]"
										value="<?=$detail[$i]['spare_part_quantity']?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">單價</label>
									<div class="col-xs-6">
										<input type="text" class="form-control"
										name="update_value[<?=$i?>][spare_price]"
										value="<?=$detail[$i]['spare_price']?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">修護內容</label>
									<div class="col-xs-10">
										<input type="text" class="form-control"
										name="update_value[<?=$i?>][working_item]"
										value="<?=$detail[$i]['working_item']?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-2 control-label">工資</label>
									<div class="col-xs-6">
										<input type="text" class="form-control"
										name="update_value[<?=$i?>][working_price]"
										value="<?=$detail[$i]['working_price']?>">
									</div>
								</div>
								<?php endfor; ?>
								<hr class="hr-primary">
	                            <div id="stuff"></div>

								<div class="form-group">
									<div class="col-xs-offset-2 col-xs-10">
										<button type="button" class="btn btn-info"
										onclick="add_stuff();">新增維修項目</button>
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-offset-2 col-xs-10">
										<button type="submit" class="btn btn-default">送出</button>
										<button type="button" class="btn btn-default"
										onclick="location.href='<?=base_url('record')?>';">返回</button>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
	    </div>
	</div>
</div>

<?php include_once("block_footer.php"); ?>

<script src="<?=base_url()?>assets/js/jquery-1.11.1.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/js/jquery.metisMenu.js"></script>
<script src="<?=base_url()?>assets/js/custom.js"></script>

<script src="<?=base_url()?>assets/js/datetimepicker/jquery.datetimepicker.js"></script>
<script>
jQuery('#datetimepicker1').datetimepicker({
    format:'Y-m-d H:i'
});

jQuery('#datetimepicker2').datetimepicker({
    format:'Y-m-d H:i'
});

function add_stuff()
{
    $('#stuff').append('<div class="form-group" style="position:relative;"><button type="button" class="btn btn-danger" style="position:absolute;top:10px;right:10px;" onclick="location.href=\'<?=base_url("record/add")?>\';">刪除段落</button><label class="col-xs-2 control-label">零件名稱</label><div class="col-xs-6"><select name="spare_id_add[]" class="form-control"><option value="">== 請選擇 ==</option><?php foreach ($spares AS $key => $value) : ?><option value="<?=$value['id']?>"><?=$value['title']?></option><?php endforeach; ?></select></div></div><div class="form-group"><label class="col-xs-2 control-label">數量</label><div class="col-xs-6"><input type="text" name="spare_part_quantity_add[]" class="form-control" placeholder="數量"></div></div><div class="form-group"><label class="col-xs-2 control-label">單價</label><div class="col-xs-6"><input type="text" name="spare_price_add[]" class="form-control" placeholder="單價"></div></div><div class="form-group"><label class="col-xs-2 control-label">修護內容</label><div class="col-xs-10"><input type="text" name="working_item_add[]" class="form-control" placeholder="修護內容"></div></div><div class="form-group"><label class="col-xs-2 control-label">工資</label><div class="col-xs-6"><input type="text" name="working_price_add[]" class="form-control" placeholder="工資"></div></div><hr class="hr-primary">');
}
</script>

</body>
</html>