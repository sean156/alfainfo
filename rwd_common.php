<?php
////////////////////////////////////////////////////
// Watch project
//
// common php
//
// Copyright (C) 2016 Sean Bao
////////////////////////////////////////////////////

/*******************
 * 資料表注意事項:
 * 新增 rwd_ 開頭資料表
 * STUFF_PIC 新增 img_file_s 欄位
 *******************/


if (!defined("IN_WBS")) {
    die("Hacking attempt");
}

ini_set("session.gc_maxlifetime", "14400");

/*if (isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"]=='on') {
    $domainUrl = DOMAIN_SSL_URL;
} else {
    $domainUrl = DOMAIN_URL;
}*/

//Disable magic_quotes_runtime
//set_magic_quotes_runtime(0);

//filter variable
/*foreach (array('GLOBALS', '_SESSION', 'HTTP_SESSION_VARS', '_GET', 'HTTP_GET_VARS', '_POST', 'HTTP_POST_VARS', '_COOKIE', 'HTTP_COOKIE_VARS', '_REQUEST', '_SERVER', '_ENV', '_FILES') as $bad_global) {
    if (isset($_REQUEST[$bad_global])) {
        header("Location: ".$_SERVER['PHP_SELF']);
        exit();
    }
}*/

foreach ($_GET as $secvalue) {
    if ((preg_match("/<[^>]*script*\"?[^>]*>/i", $secvalue)) ||
    (preg_match("/<[^>]*object*\"?[^>]*>/i", $secvalue)) ||
    (preg_match("/<[^>]*iframe*\"?[^>]*>/i", $secvalue)) ||
    (preg_match("/<[^>]*applet*\"?[^>]*>/i", $secvalue)) ||
    (preg_match("/<[^>]*meta*\"?[^>]*>/i", $secvalue)) ||
    (preg_match("/<[^>]*style*\"?[^>]*>/i", $secvalue)) ||
    (preg_match("/<[^>]*form*\"?[^>]*>/i", $secvalue)) ||
    //(eregi("\([^>]*\"?[^)]*\)", $secvalue)) ||
    (preg_match("/\"/i", $secvalue))) {
       die("<center><br><br><b>The html tags you attempted to use are not allowed</b><br><br>[ <a href=\"javascript:history.go(-1)\"><b>Go Back</b></a> ]");
    }
}

foreach ($_POST as $secvalue) {
    //if ((eregi("<[^>]*script*\"?[^>]*>", $secvalue)) || (eregi("<[^>]*style*\"?[^>]*>", $secvalue))) {
    if (is_string($secvalue)) {
        if (!preg_match("/admin2.php/", __FILE__)) { //排除後端
            if ((preg_match("/<[^>]*script*\"?[^>]*>/i", $secvalue)) ||
            (preg_match("/<[^>]*object*\"?[^>]*>/i", $secvalue)) ||
            (preg_match("/<[^>]*iframe*\"?[^>]*>/i", $secvalue)) ||
            (preg_match("/<[^>]*applet*\"?[^>]*>/i", $secvalue)) ||
            (preg_match("/<[^>]*meta*\"?[^>]*>/i", $secvalue)) ||
            (preg_match("/<[^>]*style*\"?[^>]*>/i", $secvalue)) ||
            (preg_match("/<[^>]*form*\"?[^>]*>/i", $secvalue))) {
               die("<center><br><br><b>The html tags you attempted to use are not allowed</b><br><br>[ <a href=\"javascript:history.go(-1)\"><b>Go Back</b></a> ]");
            }
        }
    }
}

include_once("rwd/includes/config.php");

//initial MySql PDO class
include_once("rwd/includes/class.pdomysql.php");
$dsn = "mysql:host=".$site['dbhost']."; dbname=".$site['dbname'];
$db = new PDOMysql($dsn, $site['dbuser'], $site['dbpasswd']);
$db->query("SET NAMES 'utf8'");

//initial smarty
require_once("rwd/includes/Smarty/libs/Smarty.class.php");
define("__SITE_ROOT", dirname(__FILE__));
$tpl = new Smarty();
$tpl->template_dir = __SITE_ROOT . "/templates/";
$tpl->compile_dir = __SITE_ROOT . "/templates/templates_c/";
$tpl->left_delimiter = '<{';
$tpl->right_delimiter = '}>';
$tpl->compile_check = true;
$tpl->debugging = true;

//assign 共用參數
$tpl->assign("site", $site);
$tpl->assign("cat_of_article", $cat_of_article); //文章分類
$tpl->assign("cat_of_banners", $cat_of_banners); //廣告區塊

//PHPMailer
require_once("rwd/includes/PHPMailer/PHPMailerAutoload.php");
$phpmail = new PHPMailer;

require_once("rwd/includes/functions.php");

//meta
//$rule = get_agreement_and_meta();
//$tpl->assign("rule", $rule);

//session save to mysql
//require_once("rwd/includes/class.sessionmanager.php");
//$session = new SessionManager($db);

if ($is_remember) {
//延長 session 時間
    start_session(14400);
} else {
    start_session();
}
