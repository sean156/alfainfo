<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>維修紀錄列表 | <?=SITE_NAME?></title>
<!-- BOOTSTRAP STYLES-->
<link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME ICONS STYLES-->
<link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
<!--CUSTOM STYLES-->
<link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" />
<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link href="<?=base_url();?>assets/css/zebra_pagination.css" rel="stylesheet" />
</head>
<body>
<div id="wrapper">
	<?php include_once("block_nav.php"); ?>
	<div id="page-wrapper" class="page-wrapper-cls">
		<div id="page-inner">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="page-head-line">維修紀錄列表</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">

					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-1">
									<button data-toggle="tooltip" title="新增" class="btn btn-default"
										role="button" onclick="location.href='<?=base_url()?>record/add';">
										<i class="fa fa-plus"></i>
									</button>
								</div>
							
								<form role="form" class="form-horizontal"
									action="" method="post" onsubmit="return checkForm(this);">
								<div class="form-group">
								<div class="col-xs-6 col-xs-offset-4">
									<div class="input-group">
										<?php echo validation_errors(); ?>
										<input type="text" class="form-control" name="keyword"
											value="<?=urldecode($this->uri->segment(3))?>">
										<span class="input-group-btn">
											<button class="btn btn-default" type="submit"><i
												class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
								</div><!-- /.form-group -->
								</form>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>工作單號</th>
											<th>哩程</th>
											<th>進廠時間</th>
											<th>離廠時間</th>
											<th colspan="2">功能</th>
										</tr>
									</thead>
									<tbody>
										<?php if (empty($records)) : ?>
										<tr>
											<td colspan="7" class="text-center">無資料</td>
										</tr>
										<?php else : ?>
											<?php foreach ($records as $key => $value) : ?>
										<tr>
											<td><?=$value['id']?></td>
											<td><a href="<?=base_url('record/detail/'.$value['working_no'])?>"
												><?=$value['working_no']?></a></td>
											<td><?=$value['mileage']?></td>
											<td><?=$value['arrive_time']?></td>
											<td><?=$value['depart_time']?></td>
											<td><button data-toggle="tooltip" title="修改"
												class="btn btn-default" role="button" onclick="location.href='<?=base_url();?>record/edit/<?=$value['id'];?><?=$this->uri->slash_segment(3, 'leading');?>';">
												<i class="fa fa-edit"></i>
											</button></td>
											<td><button data-toggle="tooltip" title="刪除"
												class="btn btn-default" role="button"
												onclick="location.href='<?=base_url("record/delete/".$value['working_no'])?>';">
												<i class="fa fa-trash-o"></i>
											</button></td>
										</tr>
											<?php endforeach; ?>
										<?php endif; ?>
									</tbody>
								</table>
							</div><!-- /.table-responsive -->
						</div><!-- /.panel-body -->
					</div><!-- /.panel -->
					<?php if (isset($page)) echo $page; ?>
				</div>
			</div>
	    </div>
	</div>
</div>

<?php include_once("block_footer.php"); ?>

<script src="<?=base_url();?>assets/js/jquery-1.11.1.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.js"></script>
<script src="<?=base_url();?>assets/js/jquery.metisMenu.js"></script>
<script src="<?=base_url();?>assets/js/custom.js"></script>
<script>
function checkForm(obj) {
	var keyword = obj.keyword.value;
//alert(keyword);
	if (keyword == "") {
		return false;
	} else {
		location.href = '<?=base_url();?>record/keyword/' + encodeURI(keyword);
		return false;
	}
}
</script>
</body>
</html>