<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public $site_setting = array();

	public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->config->load('site_setting', TRUE);
        $this->site_setting = $this->config->item('site_setting');
    }

    public function check_login()
    {
        if (empty($_SESSION['admin'])) {
            header('Location: /admin/login');
            exit();
        }
    }

    public function login()
    {
        $data['site_setting'] = $this->site_setting;
        $this->load->view('admin_login', $data);
    }

    public function index()
    {
        $this->check_login();

        $data['site_setting'] = $this->site_setting;
        $this->load->view('admin_home', $data);
    }

    public function ex_login()
    {
        $this->escape_post(); //過濾 post

        $admin_row = $this->admin_model->get_admin_by_login($_POST['account'], $_POST['pwd']);
        if (empty($admin_row)) {
        //登入失敗

            $this->redirect_header("/admin/login", "帳號密碼錯誤！");
        } else {
        //登入成功, 存進 session

            $_SESSION['admin'] = base64_encode($admin_row['no'].":".$admin_row['account']);
            header("Location: /admin");
            exit();
        }
    }

    public function escape_post()
    {
        foreach ($_POST as $key => $value) {
            $_POST[$key] = $this->db->escape_str($value);
        }
    }

    public function logout()
    {
        session_destroy();
        header("Location: /admin/login");
        exit();
    }

    /**
     * redirection
     */
    public function redirect_header($url, $message = "", $time = "0")
    {
        $url = preg_replace("/&amp;/i", '&', htmlspecialchars($url, ENT_QUOTES));
        echo '
        <!DOCTYPE html>
        <html lang="zh-Hant">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="refresh" content="'.$time.';url='.$url.'">
        <title>'.$GLOBALS['config']['site_name'].'</title>
        <style type="text/css">
            body {background-color : #fcfcfc; font-size: 12px; font-family: Trebuchet MS,Verdana, Arial, Helvetica, sans-serif; margin: 0px;}
            .redirect {width: 70%; margin: 0 auto; text-align: center; padding: 15px; border: #e0e0e0 1px solid; color: #666666; background-color: #f6f6f6;}
            .redirect a:link {color: #666666; text-decoration: none; font-weight: bold;}
            .redirect a:visited {color: #666666; text-decoration: none; font-weight: bold;}
            .redirect a:hover {color: #999999; text-decoration: underline; font-weight: bold;}
        </style>
        </head>
        <body>';

        if ($message != "") {
            echo '
            <div class="redirect">
              <span style="font-size: 16px; font-weight: bold;">'.$message.'</span>
              <hr style="height: 3px; border: 3px #FF9900 solid; width: 95%;">
              <p>If your browser does not support meta redirection, please click <a href="'.$url.'">HERE</a>
                to be redirected</p>
            </div>';
        }

        echo '
        </body>
        </html>';
        die();
    }

}