<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Record_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function getRecord($where = "", $bind = array(), $orderby = "id DESC", $limit = "")
    {
        $column_name = $this->getColumnName("repair_info");

        $sql = "SELECT ".$column_name." FROM repair_info ";
        if (!empty($where)) {
        	$sql .= " WHERE ".$where;
        }
        if (!empty($orderby)) {
        	$sql .= " ORDER BY ".$orderby;
        }
        if (!empty($limit)) {
        	$sql .= " LIMIT ".$limit;
        }

		if (empty($bind)) {
	        $query = $this->db->query($sql);
	    } else {
	    	$query = $this->db->query($sql, $bind);
	    }

        $result = $query->result_array();

        if (empty($result)) {
            return 0;
        } else {
            return $result;
        }
    }

    public function countRecord($where = "", $bind = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM repair_info ";
        if (!empty($where)) {
        	$sql .= "WHERE ".$where;
        }

        if (empty($bind)) {
	        $query = $this->db->query($sql);
	    } else {
	    	$query = $this->db->query($sql, $bind);
	    }

        $result = $query->result_array();

        if (empty($result)) {
            return 0;
        } else {
            return $result[0]['total'];
        }
    }

	public function addRecord($data) {
		$sql = "INSERT INTO repair_info SET ";

		if (empty($data)) {
			return 0;
		}

		foreach ($data AS $key => $value) {
			$sql .= $key."= ?,";
			$bind[] = $value;
		}
		$sql = substr($sql, 0, strlen($sql) - 1);

		$this->db->query($sql, $bind);

		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		} else {
			return 0;
		}
	}

	public function modifyRecord($where, $bind_where, $data) {
		$sql = "UPDATE repair_info SET ";

		if (empty($where)) {
			return 0;
		}

		foreach ($data AS $key => $value) {
			$sql .= $key."= ?,";
			$bind[] = $value;
		}

		if (!empty($bind_where)) {
			array_push($bind, $bind_where);
		}

		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= " WHERE ".$where;

		$this->db->query($sql, $bind);

		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return 0;
		}
	}

	public function modifyRecordDetail($where, $bind_where, $data) {
		$sql = "UPDATE repair_info_detail SET ";

		if (empty($where)) {
			return 0;
		}

		foreach ($data AS $key => $value) {
			$sql .= $key."= ?,";
			$bind[] = $value;
		}

		if (!empty($bind_where)) {
			array_push($bind, $bind_where);
		}

		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= " WHERE ".$where;

		$this->db->query($sql, $bind);

		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return 0;
		}
	}

    /**
     * @parameter $data array 二維陣列 $data[0]['title']
     */
    public function addRecordDetail($data)
    {
        $column = implode(",", array_keys($data[0]));

        $full_str = "";
        foreach ($data AS $key => $value) {
            $str = "(";
            $j = 0;
            foreach ($value AS $k => $v) {
                $bind[] = $v;
                if ($j == (count($value) - 1)) {
                    $str .= "?),";
                } else {
                    $str .= "?,";
                }
                $j++;
            }
            $full_str .= $str;
        }
        $full_str = substr($full_str, 0, strlen($full_str) - 1);
        $sql = "INSERT INTO repair_info_detail (".$column.") VALUES ".$full_str;

        $this->db->query($sql, $bind);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getRecordDetail($where = "", $bind = array(), $orderby = "id DESC", $limit = "")
    {
        $column_name = $this->getColumnName("repair_info_detail");

        $sql = "SELECT ".$column_name." FROM repair_info_detail ";
        if (!empty($where)) {
        	$sql .= " WHERE ".$where;
        }
        if (!empty($orderby)) {
        	$sql .= " ORDER BY ".$orderby;
        }
        if (!empty($limit)) {
        	$sql .= " LIMIT ".$limit;
        }

		if (empty($bind)) {
	        $query = $this->db->query($sql);
	    } else {
	    	$query = $this->db->query($sql, $bind);
	    }

        $result = $query->result_array();

        if (empty($result)) {
            return 0;
        } else {
            return $result;
        }
    }

	public function deleteRecord($where, $bind) {
		if (empty($where)) {
			return 0;
		} else {
			//先刪除 detail
			$sql0 = "DELETE FROM repair_info_detail WHERE ".$where;
			$this->db->query($sql0, $bind);

			$sql = "DELETE FROM repair_info WHERE ".$where;
			$this->db->query($sql, $bind);

			if ($this->db->affected_rows() > 0) {
				return $this->db->affected_rows();
			} else {
				return 0;
			}
		}
	}

    public function getColumnName($table_name)
    {
    	$column_name = "";
    	$sql = "SHOW COLUMNS FROM ".$table_name;
		$query = $this->db->query($sql);
		$result = $query->result_array();

		if (!empty($result)) {
			foreach($result as $key => $value) {
				$column_name .= $value['Field'].',';
			}
			$column_name = substr($column_name, 0, strlen($column_name) - 1);
		}

		return $column_name;
	}
}