<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("Users.php");

class Spare extends Users {

	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
		$this->load->model("spare_model");
		//$this->load->library('zebra_pagination');
	}

	public function index($keyword = "")
	{
		$this->load->library("zebra_pagination");
		//$this->form_validation->set_rules('code', '代號', 'required');

		$where = "";
		$bind = array();
		$orderby = "id DESC";

		//search
		//if (!empty($_GET)) {
		//	$keyword = $_GET['keyword'];
		//}

		if (!empty($keyword)) {
			$where .= "title LIKE ? OR code LIKE ?";
			$bind[] = "%".$keyword."%";
			$bind[] = "%".$keyword."%";
		}

		//how many records should be displayed on a page?
		$records_per_page = 5;

		$this->zebra_pagination->padding(false);
		$this->zebra_pagination->method('url');
		$limit = (($this->zebra_pagination->get_page() - 1) * $records_per_page).", ".$records_per_page;
//echo "p=".$this->zebra_pagination->get_page();
		$data['spares'] = $this->spare_model->getSpare($where, $bind, $orderby, $limit);
		$total = $this->spare_model->countSpare($where, $bind);
//echo "w=".$where;
//print_r($bind);
		if ($total > 0) {
		    //pass the total number of records to the pagination class
		    $this->zebra_pagination->records($total);

		    //records per page
		    $this->zebra_pagination->records_per_page($records_per_page);

		    //render the pagination links
		    $page = $this->zebra_pagination->render(true);

		    $data['page'] = $page;
		}

		$this->load->view('spare_list', $data);
	}

	public function add()
	{
		$this->load->view("spare_add");
	}

	public function create() {
		$this->form_validation->set_rules("title", "名稱", "required");
		$this->form_validation->set_rules("code", "代號", "required");
		
		if ($this->form_validation->run() === FALSE) {
			$this->load->view("spare_add");
		} else {
			$this->escapePost();
			$data['title'] = $_POST['title'];
			$data['code'] = $_POST['code'];
			$data['ctime'] = date("Y-m-d H:i:s");

			if ($this->spare_model->addSpare($data)) {
				redirect(base_url("spare"), "location");
			} else {
				echo "error";
			}
		}
	}

	public function edit($no)
	{
		$where = "id = ?";
		$bind = array(0 => $no);

		$result_array = $this->spare_model->getSpare($where, $bind);
		$data['row'] = $result_array[0];

		$this->load->view("spare_edit", $data);
	}

	public function modify() {
			$this->escapePost();
			$data['title'] = $_POST['title'];
			$data['code'] = $_POST['code'];
			$data['id'] = $_POST['id'];
			$data['mtime'] = date("Y-m-d H:i:s");
			//$data['mtime'] = "NOW()";

		$this->form_validation->set_rules("title", "名稱", "required");
		$this->form_validation->set_rules("code", "代號", "required");
		
		if ($this->form_validation->run() === FALSE) {
			$this->load->view("spare_edit", $data);
		} else {
			$where = "id = ?";
			$bind = array(0 => $_POST['id']);
			if ($this->spare_model->modifySpare($where, $bind, $data)) {
				redirect(base_url("spare"), "location");
			} else {
				echo "error";
			}
		}
	}

	/*public function escapePost()
    {
        foreach ($_POST as $key => $value) {
            $_POST[$key] = $this->db->escape_str($value);
        }
    }

    public function escapeGet()
    {
        foreach ($_GET as $key => $value) {
            $_GET[$key] = $this->db->escape_str($value);
        }
    }*/

    public function keyword($keyword)
    {
//echo "k=".$keyword;
		$keyword = urldecode($keyword);
    	$this->index($keyword);
    }
}